angular
  .module('garageDetails')
  .component('garageDetails', {
    template:
      '<div class="garage-details container padding-vertical">' +
        '<aside>' +
          '<div class="btn btn-default aligned-row" ng-click="$ctrl.goBack()">' +
            '<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>' +
            '<span>Back to list</span>' +
          '</div>' +
          '<traffic-light class="aligned-row" label="Short:" free="{{$ctrl.garage.freeSpaceShort}}" max="{{$ctrl.garage.capacityShort}}"></traffic-light>' +
          '<traffic-light class="aligned-row" label="Long:" free="{{$ctrl.garage.freeSpaceLong}}" max="{{$ctrl.garage.capacityLong}}"></traffic-light>' +
          '<updated-at class="aligned-row" time="$ctrl.garage.updatedAt"></updated-at>' +
        '</aside>' +
        '<main class="padding-vertical">' +
          '<map object="$ctrl.garage" />' +
        '</main>' +
      '</div>',
    controller: [
      '$window',
      '$routeParams',
      'Garage',
      function($window, $routeParams, Garage) {
        var $ctrl = this;

        $ctrl.garage = null;

        $ctrl.$onInit = function() {
          Garage.get({garageId: $routeParams.garageId}, function(garage) {
            $ctrl.garage = garage;
          });
        };

        $ctrl.goBack = function() {
          $window.history.back();
        };
      }
    ]
  });

