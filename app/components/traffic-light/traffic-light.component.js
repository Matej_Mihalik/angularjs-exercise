angular
  .module('trafficLight')
  .component('trafficLight', {
    template:
      '<span class="light" ng-class="$ctrl.free ? ($ctrl.free === \'0\' ? \'red\' : \'green\') : \'\'"></span>' +
      '<span>{{$ctrl.label}}</span>' +
      '<span>{{$ctrl.free !== "" ? $ctrl.free + " / " + $ctrl.max : "N/A"}}</span>',
    bindings: {
      label: '@',
      free: '@',
      max: '@'
    }
  });
