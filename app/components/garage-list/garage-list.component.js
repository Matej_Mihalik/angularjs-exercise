angular
  .module('garageList')
  .component('garageList', {
    template:
      '<div class="container padding-vertical">' +
        '<main class="garage-list">' +
          '<div class="row">' +
            '<div class="col-sm-6 col-md-4 col-lg-3" ng-repeat="garage in $ctrl.garages">' +
              '<a href="#!/garages/{{garage.id}}" class="garage panel panel-primary">' +
                '<div class="panel-heading">' +
                  '<h3 class="panel-title">{{garage.title}}</h3>' +
                '</div>' +
                '<div class="panel-body">' +
                  '<traffic-light class="aligned-row" label="Short:" free="{{garage.freeSpaceShort}}" max="{{garage.capacityShort}}"></traffic-light>' +
                  '<traffic-light class="aligned-row" label="Long:" free="{{garage.freeSpaceLong}}" max="{{garage.capacityLong}}"></traffic-light>' +
                  '<updated-at class="aligned-row" time="garage.updatedAt"></updated-at>' +
                '</div>' +
              '</a>' +
            '</div>' +
          '</div>' +
        '</main>' +
        '<aside class="text-center" ng-if="$ctrl.hasMore">' +
          '<button type="button" class="btn btn-primary btn-lg" ng-click="$ctrl.getNextPage()">Load more</button>' +
        '</aside>' +
      '</div>',
    controller: [
      'Garage',
      function(Garage) {
        var $ctrl = this;

        $ctrl.page = 0;
        $ctrl.pageLength = 12;
        $ctrl.hasMore = false;
        $ctrl.garages = [];

        $ctrl.$onInit = function() {
          $ctrl.getNextPage();
        };

        $ctrl.getNextPage = function() {
          Garage.query({
            page: ++$ctrl.page,
            per_page: $ctrl.pageLength
          }, function(garages) {
            $ctrl.garages = $ctrl.garages.concat(garages);
            $ctrl.hasMore = $ctrl.garages.length === $ctrl.page * $ctrl.pageLength;
          });
        };
      }
    ]
  });
