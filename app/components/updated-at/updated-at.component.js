angular
  .module('updatedAt')
  .component('updatedAt', {
    template: '<em class="text-faded"><small>Last updated: {{$ctrl.time ? $ctrl.time.toLocaleString() : "N/A"}}</small></em>',
    bindings: {
      time: '<'
    }
  });
