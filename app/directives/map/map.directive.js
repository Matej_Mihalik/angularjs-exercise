angular
  .module('map')
  .directive('map', function() {
    function initMap(object, container) {
      var position = {
        lat: object.lat,
        lng: object.lng
      };
      var map = new google.maps.Map(container, {
        center: position,
        zoom: 15
      });
      var marker = new google.maps.Marker({
        position: position,
        map: map
      });
    };

    return {
      scope: {
        object: '<'
      },
      link: function(scope, element) {
        scope.$watch('object', function(object) {
          if(object) {
            initMap(object, element[0]);
          }
        });
      }
    }
  });
