angular
  .module('garageViewerApp')
  .config([
    '$locationProvider',
    '$routeProvider',
    function ($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');
      $routeProvider
        .when('/garages', {
          template: '<garage-list />'
        })
        .when('/garages/:garageId', {
          template: '<garage-details />'
        })
        .otherwise('/garages');
    }
  ]);
