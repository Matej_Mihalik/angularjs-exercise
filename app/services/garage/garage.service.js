angular
  .module('garage')
  .factory('Garage', [
    '$resource',
    function($resource) {
      function parseFeature(feature) {
        var properties = feature.properties;
        var geometry = feature.geometry;
        var data = properties.layers['parking.garage'].data;
        return {
          id: properties.cdk_id,
          title: properties.title,
          lng: geometry.coordinates[0],
          lat: geometry.coordinates[1],
          updatedAt: 'PubDate' in data ? new Date(data.PubDate) : null,
          freeSpaceShort: 'FreeSpaceShort' in data ? data.FreeSpaceShort : null,
          freeSpaceLong: 'FreeSpaceLong' in data ? data.FreeSpaceLong : null,
          capacityShort: 'ShortCapacity' in data ? data.ShortCapacity : null,
          capacityLong: 'LongCapacity' in data ? data.LongCapacity : null
        };
      }

      return $resource('http://api.citysdk.waag.org/layers/parking.garage/objects', {}, {
        get: {
          url: 'http://api.citysdk.waag.org/objects/:garageId',
          method: 'GET',
          isObject: true,
          transformResponse: function(data) {
            var json = angular.fromJson(data);
            return parseFeature(json.features[0]);
          }
        },
        query: {
          method: 'GET',
          isArray: true,
          transformResponse: function(data) {
            var json = angular.fromJson(data);
            return json.features.map(parseFeature);
          }
        },
      });
    }
  ]);
