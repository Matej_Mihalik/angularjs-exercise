# Garage Viever

### What is this?
Backbase FE Exercise 2.5 - built using Angular.js v1.5.11 & Bootstrap v3.3.7

### How to run
Just open the `app/index.html` file in your browser.
Please note that internet connection IS required, since we leverage CitySDK Linked Data API and Google Maps Javascript API.

### What's what
* `app` - the folder where our source code can be found
  * `components` - a place for all of our app's components
    * `garage-details` - a component that display the details of a chosen garage. We read the garage id from the route params in order to retrieve data for the correct garage. While inlined template of this size is especially ugly, we've decided to inline it in order to make sure our app runs simply by opening the index file in the browser. Ideally we would keep the templates in a separate file and provide a route to it via _templateUrl_. But again, that would require our app to be deployed on a server first.
    * `garage-list` - a component that display the list of existing garages. Originally we intended to have a pagination in order to move through the list of available garages. Unfortunately the CitySDK only exposes the required pagination info through response headers, which aren't visible in JS (My best guess is that the API server doesn't expose them for CORS requests). So we went with a simpler _Load more_ button which can work reasonably well without any pagination info. And again, inlined template here even though it's ugly.
    * `traffic-light` - a small reusable component that takes three params - _label_, _free_ and _max_. It display a traffic light (green, red or gray), depending on the passed params. Eg. if there are free parking spots, the light is green, if there are not, the light is red. If the current number of available parking spaces could not be determined, the light is gray.
    * `updated-at` - another small reusable component that displays when was the information we have last updated. It needs a single `time` param. It is expected the param will be a JS Date object. We use a _toLocaleString_ method to format the date. If we require more robust formatting options in the future, we could leverage another third party library, like _moment.js_.
  * `directives` - a place for all of our app's directives
    * `map` - a directive that displays a responsive map leveraging the Google Maps API. It requires a single _object_ param. The object needs to have _lat_ and _lng_ properties. If the object is _null_, no map will be drawn.
  * `services` - a place for all of our app's directives
    * `garage` - here we define the service for retrieving Garage resources leveraging the CitySDK API. We also massage the data into a flat object structure more suitable for our needs.
  * `app.config.js` - main config file for our app, where we configure the routing
  * `app.module.js` - register the main app module here
  * `index.html` - the one and only entrypoint into our app. The scripts are intentionally left unminified (apart from third party libs), uncompressed and uncombined for clearer dependency overview.
  * `style.css` - stylesheet for our app. Since the styling is rather lightweight, we've decided to store all our css here.
* `lib` - the folder where we store our third party dependencies. Namely, this is Bootstrap for UI styling, and Angular (with resource and route extensions) to power our app.
* `README.md` - the file you are reading right now
